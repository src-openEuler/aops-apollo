Name:		aops-apollo
Version:	v2.0.0
Release:	5
Summary:	Cve management service, monitor machine vulnerabilities and provide fix functions.
License:	MulanPSL2
URL:		https://gitee.com/openeuler/%{name}
Source0:	%{name}-%{version}.tar.gz
Patch0001:  0001-fix-security-advisory-parsing-error.patch
Patch0002:  0002-fix-bug-with-host-count-in-cve-fix-task.patch
Patch0003:  0003-fix-issue-with-language-display-in-task-generation.patch
Patch0004:  0004-fix-repo-query-error-and-adjust-schema.patch

BuildRequires:  python3-setuptools
Requires:   aops-vulcanus >= v2.0.0
Requires:   python3-gevent python3-uWSGI python3-celery aops-zeus >= v2.0.0
Provides:   aops-apollo


%description
Cve management service, monitor machine vulnerabilities and provide fix functions.


%package -n aops-apollo-tool
Summary: Small tools for aops-apollo, e.g. updateinfo.xml generater
Requires: python3-rpm

%description -n aops-apollo-tool
smalltools for aops-apollo, e.g.updateinfo.xml generater

%prep
%autosetup -n %{name}-%{version} -p1


# build for aops-apollo
%py3_build

# build for aops-apollo-tool
pushd aops-apollo-tool
%py3_build
popd

# install for aops-apollo
%py3_install
mkdir -p %{buildroot}/opt/aops/
cp -r database %{buildroot}/opt/aops/

# install for aops-apollo-tool
pushd aops-apollo-tool
%py3_install
popd


%files
%doc README.*
%attr(0644,root,root) %{_sysconfdir}/aops/conf.d/aops-apollo.yml
%attr(0755,root,root) %{_unitdir}/aops-apollo.service
%{python3_sitelib}/aops_apollo*.egg-info/*
%{python3_sitelib}/apollo/*
%attr(0755, root, root) /opt/aops/database/*


%files -n aops-apollo-tool
%attr(0644,root,root) %{_sysconfdir}/aops_apollo_tool/updateinfo_config.ini
%attr(0755,root,root) %{_bindir}/gen-updateinfo
%{python3_sitelib}/aops_apollo_tool*.egg-info/*
%{python3_sitelib}/aops_apollo_tool/*

%changelog
* Mon Sep 09 2024 wenxin<wenxin32@foxmail.com> - v2.0.0-5
- Fix issue with querying repo info api
- Adjust some schema validation logic

* Fri Aug 30 2024 wenxin<wenxin32@foxmail.com> - v2.0.0-4
- Fix issue with language display in task generation api

* Fri Aug 16 2024 wenxin<wenxin32@foxmail.com> - v2.0.0-3
- Added support for cluster features.
- Adjusted Task Module logic to use Celery for task management and execution.

* Fri Dec 22 2023 wenxin<wenxin32@foxmail.com> - v1.4.1-3
- fix the query error of cve associated host
- update verification method for host ip fieldl;fix repo field filter error
- update TimedCorrectTask method

* Mon Dec 18 2023 luxuexian<luxuexian@huawei.com> - v1.4.1-2
- fix cve_list sort order

* Mon Dec 18 2023 wenxin<wenxin32@foxmail.com> - v1.4.1-1
- Add support for CVE rollback tasks
- Optimize the code and adjust the project structure

* Tue Dec 12 2023 wangguangge<wangguangge@huawei.com> - v1.4.0-1
- Refactoring repair tasks and hot patch removal tasks

* Tue Nov 14 2023 wangguangge<wangguangge@huawei.com> - v1.3.4-10
- fix filename bug in aops-apollo-tool

* Mon Nov 13 2023 wenxin<wenxin32@foxmail.com> - v1.3.4-9
- Update the installation dependencies

* Tue Oct 24 2023 wenxin<wenxin32@foxmail.com> - v1.3.4-8
- fix data correction task execution error

* Mon Oct 23 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-7
- fixed the problem that the number of hosts in the cve list repeated statistics

* Mon Oct 23 2023 wenxin<wenxin32@foxmail.com> - v1.3.4-6
- fix the severity field filtering error

* Mon Oct 23 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-5
- fixed many issues with cvelist queries (package fuzzy matching, page confusion, sorting is not supported) and rpm packet loss when generating repair tasks

* Fri Oct 20 2023 wenxin<wenxin32@foxmail.com> - v1.3.4-4
- fix cve_list_get api query error

* Fri Oct 20 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-3
- fix query all by cve list api

* Fri Oct 20 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-2
- fix param error and compatible with mysql 5.7

* Thu Oct 19 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-1
- Remove hotpatch

* Wed Oct 18 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.3-2
- optimize cve query performance
- fixed errors in 20.03-sp3, such as task progress, cve repair task, and host cve query

* Thu Sep 21 2023 zhuyuncheng<zhuyuncheng@huawei.com> - v1.3.3-1
- update typing and requires version

* Thu Sep 21 2023 wangguangge<wangguangge@huawei.com> - v1.3.2-6
- fix the hot_updateinfo.py bug

* Wed Sep 20 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.2-5
- add fixed and hp_status filter

* Wed Sep 20 2023 wangguangge<wangguangge@huawei.com> - v1.3.2-4
- fix the hotupgrade.py bug

* Tue Sep 19 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.2-3
- added the repair status of the cve fixed package

* Tue Sep 19 2023 wangguangge<wangguangge@huawei.com> - v1.3.2-2
- fix the updateinfo_parse.py bug

* Tue Sep 19 2023 wenxin<shusheng.wen@outlook.com> - v1.3.2-1
- fix cve scan callback error
- fix cve-fix task generate error when it only contain coldpatches
- add a method to querying fixed cve info for dnf plugin

* Wed Sep 13 2023 zhuyuncheng<zhuyuncheng@huawei.com> -v1.3.1-5
- fix task_cve_host return all host bug

* Wed Sep 13 2023 gongzhengtang<gong_zhengtang@163.com> -v1.3.1-4
- fixed host ip addresses are not verified in the generation task

* Mon Sep 11 2023 gongzhengtang<gong_zhengtang@163.com> -v1.3.1-3
- fixed several known issues
- fix dnf hot-updateinfo list cves bug
- fixed an error generated after selecting a specific rpm package

* Tue Sep 5 2023 zhuyuncheng<zhuyuncheng@huawei.com> - v1.3.1-2
- fix bug: delete host id filter when rollback in cve list interface

* Tue Sep 5 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.1-1
- cve repair tasks support rpm packet granularity

* Tue Aug 29 2023 wangguangge<wangguangge@huawei.com> - v1.3.0-1
- update the dnf hot-updateinfo, dnf hotpatch and dnf hotupgrade command
- support the mixed management ability for coldpatches and hotpatches   

* Fri Jun 30 2023 wenxin<shusheng.wen@outlook.com> - v1.2.2-4
- Update the condition under which hot patches can be applied

* Wed Jun 28 2023 wangguangge<wangguangge@huawei.com> - v1.2.2-3
- do not return the related hotpatches when the cve is fixed

* Wed Jun 28 2023 wenxin<shusheng.wen@outlook.com> - v1.2.2-2
- fix issue:gen cve task failed

* Tue Jun 20 2023 gongzhengtang<gong_zhengtang@163.com> - v1.2.2-1
- Fixes numerous known issues

* Mon Jun 12 2023 wangguangge<wangguangge@huawei.com> - v1.2.1-6
- modify the interface of get_hotpatches_from_cve

* Fri Jun 09 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-5
- fix issue: API return 500 when create cve fix task without parameter auto_reboot

* Thu Jun 08 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-4
- fix issue: hotpatch status filter exception
- update validation rules for paging parameters

* Fri Jun 2 2023 gongzhengtang<gong_zhengtang@163.com> - v1.2.1-3
- fix bug and update the code of parsing src.rpm
- fix hotpatch updateinfo for search hotpatch information
- add dnf full repair
- the host and cve were not verified when the generate task was fixed
- update hotpatch status related operation support

* Wed May 31 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-2
- fix issue that can not be filtered by CVE ID when query cve rollbak task info
- fix issue that can not be filtered by cve rollback when query task list
- fix issue that can not be filtered by hotpatch when query host cve info
- fix issue that fail to read cve information when all realted hosts have been fixed

* Tue May 23 2023 zhu-yuncheng<zhuyuncheng@huawei.com> - v1.2.1-1
- Better dnf hotpatch plugin for more syscare command
- Add updateinfo.xml generation tool

* Thu May 11 2023 ptyang<1475324955@qq.com> - v1.2.0-4
- Add network request exception capture

* Tue May 9 2023 ptyang<1475324955@qq.com> - v1.2.0-3
- fix send two emails bug

* Thu Apr 27 2023 ptyang<1475324955@qq.com> - v1.2.0-2
- fix args not effective bug
- download SA using a collaborative process

* Mon Apr 17 2023 gongzhengtang<gong_zhengtang@163.com> - v1.2.0-1
- add updated security advisory at regular time
- add execute the CVE scan command at regular time
- add correct abnormal data at regular time
- add dnf hotpatch list plugin

* Tue Dec 27 2022 wenxin<shusheng.wen@outlook.com> - v1.1.2-3
- modify version for vulcanus

* Thu Dec 15 2022 ptyang<1475324955@qq.com> - v1.1.2-2
- fix "PARTIAL_SUCCEED" bug

* Wed Dec 07 2022 wenxin<shusheng.wen@outlook.com> - v1.1.2-1
- modify status code for upload security advisories;fix cve query error

* Mon Dec 05 2022 gongzhengtang<gong_zhengtang@163.com> - v1.1.1-3
- Avoid the occasional 500 or query error when the api
- service is started through uwsgi

* Fri Dec 02 2022 gongzhengtang<gong_zhengtang@163.com> - v1.1.1-2
- fix param length validate and other bugs

* Fri Dec 02 2022 wenxin<shusheng.wen@outlook.com> - v1.1.1-1
- fix some bugs

* Sat Nov 26 2022 gongzhengtang<gong_zhengtang@163.com> - v1.1.0-2
- Fix param limit of length

* Fri Nov 25 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-1
- version update

* Wed Oct 19 2022 zhuyuncheng<zhuyuncheng@huawei.com> - v1.0.0-1
- Package init
